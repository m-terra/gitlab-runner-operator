#!/usr/bin/env python3

import sys
import image
import subprocess
import config
import dockerutil
import configutil
import fileutil
import tools
from ruamel import yaml


def operator_image(push):
    configutil.write_release_config()

    arch = config.arch()
    operator_image_tag = image.get(image.IMAGE_NAME_OPERATOR, arch)
    print(f"Building operator image {operator_image_tag}")

    repository = config.upstream_operator_images_repository()
    version = config.operator_version()
    os = config.os()
    subprocess.run(["make",
                    f"PROJECT={repository}",
                    f"VERSION={version}",
                    f"ARCH={arch}",
                    f"OS={os}",
                    "REVISION=",
                    "docker-build",
                    "docker-tag"
                    ]).check_returncode()

    if push:
        dockerutil.push(operator_image_tag)


def manifest(resource, push):
    manifest_tag = image.get(image.resource_to_image(resource))
    print(f"Creating manifests {manifest_tag}")

    manifest, tags = image.manifest(manifest_tag)
    dockerutil.manifest_create(manifest, tags)
    if push:
        dockerutil.manifest_push(manifest)


def bundle(push):
    operator_image = image.get(image.IMAGE_NAME_OPERATOR)
    print(f"Building bundle for operator image {operator_image}")

    subprocess.run(["make", f"IMG={operator_image}", "bundle"])
    configutil.write_related_images()
    subprocess.run([fileutil.resove_file_path("../update_csv_fields.sh"),
                    config.operator_version()])

    # REVISION is already included in the $OPERATOR_VERSION that was passed down from the initial make target
    # TODO: transform all these make targets to python code
    subprocess.run([
        "make",
        "PROJECT={}".format(config.upstream_operator_images_repository()),
        "VERSION={}".format(config.operator_version()),
        "REVISION=",
        "certification-bundle",
        "certification-bundle-build",
        "certification-bundle-tag"
    ])

    if push:
        dockerutil.push(image.get(image.IMAGE_NAME_BUNDLE))


def catalog(push):
    bundle_image = image.get(image.IMAGE_NAME_BUNDLE)

    arch = config.arch()
    catalog_image = image.get(image.IMAGE_NAME_CATALOG, arch)
    opm_image = config.opm_binary_image(arch)

    subprocess.run(
        [tools.opm(), "index", "add", f"--bundles={bundle_image}", "--mode=semver", f"--tag={catalog_image}",
            "--build-tool=docker", f"--binary-image={opm_image}"]
    ).check_returncode()

    if push:
        dockerutil.push(catalog_image)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Specify operation to run")
        exit(1)

    operation = sys.argv[1]
    sys.argv.remove(operation)

    push = False
    if "--push" in sys.argv:
        push = True
        sys.argv.remove("--push")

    if operation == "operator-image":
        operator_image(push)
    elif operation == "bundle":
        bundle(push)
    elif operation == "manifest":
        resource = sys.argv[1]
        sys.argv.remove(resource)
        manifest(resource, push)
    elif operation == "catalog":
        catalog(push)
