package runner

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	appsv1 "k8s.io/api/apps/v1"

	"gotest.tools/assert"

	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getTestRunner() *gitlabv1beta2.Runner {
	return &gitlabv1beta2.Runner{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test-runner",
			Namespace: "default",
			Labels: map[string]string{
				"purpose": "test",
			},
		},
		Spec: gitlabv1beta2.RunnerSpec{
			GitLab:            "https://gitlab.com",
			RegistrationToken: "runner-token-secret",
			Tags:              "openshift, test",
			HelperImage:       "gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper-ubi:latest",
			BuildImage:        "ubuntu:20.04",
		},
	}
}

func TestDeployment(t *testing.T) {
	mockErr := errors.New("mockErr")

	tests := map[string]struct {
		setupCtrl        func(t *testing.T, ctrl *Controller)
		assertDeployment func(t *testing.T, runnerSpec gitlabv1beta2.RunnerSpec, deployment *appsv1.Deployment)
		err              error
	}{
		"default runner image": {
			setupCtrl: func(t *testing.T, ctrl *Controller) {
				ctrl.cr.Spec = gitlabv1beta2.RunnerSpec{}

				imageResolverMock := &mockImageResolver{}
				imageResolverMock.On("RunnerImage").Return("default", nil)
				imageResolverMock.On("HelperImage").Return("default", nil)
				ctrl.imageResolver = imageResolverMock
			},
			assertDeployment: func(t *testing.T, runnerSpec gitlabv1beta2.RunnerSpec, deployment *appsv1.Deployment) {
				spec := deployment.Spec.Template.Spec
				assert.Equal(t, spec.InitContainers[0].Image, "default")
				assert.Equal(t, spec.Containers[0].Image, "default")
			},
		},
		"default runner image error": {
			setupCtrl: func(t *testing.T, ctrl *Controller) {
				ctrl.cr.Spec = gitlabv1beta2.RunnerSpec{}

				imageResolverMock := &mockImageResolver{}
				imageResolverMock.On("HelperImage").Return("default", nil)
				imageResolverMock.On("RunnerImage").Return("", mockErr)
				ctrl.imageResolver = imageResolverMock
			},
			err: mockErr,
		},
		"default runner helper image error": {
			setupCtrl: func(t *testing.T, ctrl *Controller) {
				ctrl.cr.Spec = gitlabv1beta2.RunnerSpec{}

				imageResolverMock := &mockImageResolver{}
				imageResolverMock.On("HelperImage").Return("", mockErr)
				ctrl.imageResolver = imageResolverMock
			},
			err: mockErr,
		},
		"custom runner image": {
			setupCtrl: func(t *testing.T, ctrl *Controller) {
				ctrl.cr.Spec = gitlabv1beta2.RunnerSpec{RunnerImage: "custom"}

				imageResolverMock := &mockImageResolver{}
				imageResolverMock.On("HelperImage").Return("default", nil)

				ctrl.imageResolver = imageResolverMock
			},
			assertDeployment: func(t *testing.T, runnerSpec gitlabv1beta2.RunnerSpec, deployment *appsv1.Deployment) {
				spec := deployment.Spec.Template.Spec
				assert.Equal(t, spec.InitContainers[0].Image, runnerSpec.RunnerImage)
				assert.Equal(t, spec.Containers[0].Image, runnerSpec.RunnerImage)
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			ctrl := New(getTestRunner(), false)
			tt.setupCtrl(t, ctrl)

			deployment, err := ctrl.Deployment(nil)
			if tt.err != nil {
				require.ErrorIs(t, err, tt.err)
			} else {
				tt.assertDeployment(t, ctrl.cr.Spec, deployment)
			}
		})
	}
}
